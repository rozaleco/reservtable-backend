"use strict";

const bcrypt = require('bcryptjs');

module.exports = function (sequelize, DataTypes) {
    return sequelize.define("cms_user", {
        email: {type: DataTypes.STRING, unique: {msg: 'Email already exist.'}},
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            set(password) {
                this.setDataValue('password', bcrypt.hashSync(password, bcrypt.genSaltSync(8), null));
            },
        },
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING
    }, {
        instanceMethods: {
            validPassword: function (password) {
                return bcrypt.compareSync(password, this.password);
            },
        }
    });
};
